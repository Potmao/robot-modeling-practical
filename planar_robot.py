"""
Simulation of a 2-link planar robot.

The current version of this file only simulates a 1-link planar robot.
Change it to simulate a 2-link planar robot, that makes a square trajectory.

We have prepared already some functions and code-examples, but you have to put
everything together. Carefully read the code and the comments. Add comments of 
your own to make your code not just readable to you self, but also to
your colleagues.
"""

# load the modules as discussed last week
import sys
from pyqtgraph.Qt import QtCore, QtWidgets
import pyqtgraph.opengl as gl
import numpy as np

from rm_utils import *

class ArmLink:
    '''This class generates a link for a robot, complete with cillinders, boxen and 
    frames(frame_begin & frame). You don't have to change anything in here'''

    def __init__(self,id, a, frame_attach) -> None:
        self.id = id
        self.armLength = a
        # color specification in (normalised) RGBA values
        self.joint_color = (1.,1.,.4,1)  #yellow
        self.arm_color   = (0.4,0.4,1,1) #light blue

        # joint and link dimensions 
        radius = 0.2
        width = .6 * 2 * radius
        depth = width 
        # make cylinder slightly larger in depth 
        # (distance along z-axis) for better visibility
        depth_cylinder = 1.2*width 

        # create the vertices and faces for the joint and link
        # that will used below to create the cylinder and box in pyqtgraph
        self.vertices_joint,    self.faces_joint    = cylinder(radius,depth_cylinder,N=40)
        self.vertices_arm,      self.faces_arm      = box((a,width,depth))

        # the frame of the link attaches to the previous frame (frame_attach), which becomes its parent frame.
        # it will move relative to that frame
        self.frame = gl.GLAxisItem(antialias=True,glOptions='opaque')
        self.frame.setParentItem(frame_attach)
        self.frame.translate(a,0,0)
        # frame_begin is a visual aid to show the relative motion compared to its parent.
        # you can outcomment it if you don't like it 
        self.frame_begin = gl.GLAxisItem(antialias=True,glOptions='opaque')
        self.frame_begin.setParentItem(self.frame)
        self.frame_begin.translate(-a,0,0)

        # now create the joint and  arm  using the previously calculated vertices and faces
        self.joint = gl.GLMeshItem(vertexes=self.vertices_joint,faces=self.faces_joint,
                    drawEdges=False,drawFaces=True,color=self.joint_color)

        self.arm = gl.GLMeshItem(vertexes=self.vertices_arm,faces=self.faces_arm,
                drawEdges=False, drawFaces=True,color=self.arm_color)

        # we specify the joint and arm relative to relative to frame, so that it will move with frame
        self.joint.setParentItem(self.frame)
        self.arm.setParentItem(self.frame)
        self.joint.translate(-a,0,0)
        self.arm.translate(-a,-width/2,0)

        return 

class Robot:
    '''
    This is your Robot class. In it you will define your space and robot.
    '''
    
    def __init__(self):
        self.w = gl.GLViewWidget()
        self.w.show()
        self.w.setWindowTitle('Planar 2-link robot simulation')
        # for now, we are just working in 2 dimensions rather than 3
        # so rotate window, such that for the fixed-world coordinate
        # frame, the x-axis (blue) is horizontal and the y-axis (yellow) 
        # is vertical (z-axis (green) is pointing towards you)
        self.w.opts['elevation'] = 90
        self.w.opts['azimuth'] = -90

        # make very small field of view, such that you don't 
        # get perspective view, and you mimic an orthogonal projection
        self.w.opts['fov'] = 1
        # to get a good image size, we should take a large distance now
        self.w.setCameraPosition(distance=500)

        # create a grid-plane:
        g = gl.GLGridItem()
        # move the grid slightly to the back
        # so the gridlines will not distort the axes of the
        # coordinate frames
        g.translate(0,0,-0.1)
        self.w.addItem(g)

        # create frame0 (the fixed world-frame)
        frame0 = gl.GLAxisItem(antialias=True,glOptions='opaque')
        # we make the lines a bit thicker to stress this is the world-frame
        frame0.setGLOptions({'glLineWidth':(2,)})
        self.w.addItem(frame0)

        # arm lengths:
        L_arm1 = 2
        L_arm2 = 1.5
        
        self.linklist = self.createRobot(frame0, L_arm1, self.w)
        self.trajectory = self.createTrajectory(L_arm1)

        #ASSIGMENT: change the above two functions so they can receive both arm lengths
 
        return

    def createRobot(self, frame0, a1, w):
        # create the linkages
        link1 = ArmLink(1,a1,frame0)   
        linkList = [link1]

        #ASSIGMENT: make the 2-link robot by adding an Armlink to the linkList
        #           Make sure you send the right frame to the Armlink!

        # now all objects are added to the window so we really can see them
        for L in linkList:
            w.addItem(L.frame_begin)
            w.addItem(L.frame)
            w.addItem(L.joint)
            w.addItem(L.arm)

        return linkList

    def createTrajectory(self,a1):
        # 1-link trajectory (trajectory is a circle)
        N = 20
        t = np.linspace(0,2*np.pi,N,endpoint=False).reshape((N,1))
        trajectory = np.hstack((a1*np.cos(t),a1*np.sin(t)))

        # 2-link trajectory (planar robot) 
        # trajectory = np.array([
        #     [a1,0],
        #     [a1+.5*a2,0],
        #     [a1+.5*a2,a2],
        #     [a1,a2],
        #     ]) 
        # N = trajectory.shape[0]
        return trajectory

class Main:
    '''
    This is the main of you script. This will initialise the widget and start the timer. it also contains the update_window
    '''
    def __init__(self):
        self.app = QtWidgets.QApplication([])
        self.Allan = Robot() #Allan is the name of my robot :), you can give it you own name if you like
        self.w = self.Allan.w
        self.i = 0
        self.linklist = self.Allan.linklist
        self.trajectory = self.Allan.trajectory
        #check whether the trajectory looks fine 
        print(self.trajectory)   

        # initialize the timer
        self.timer = QtCore.QTimer()
        # connect the update_window() function to the timer
        self.timer.timeout.connect(self.update_window)
        # start the timer, that triggers every 500 milliseconds.
        self.timer.start(500)
        self.app.exec_()
        return
    
    def update_window(self):
        N = len(self.trajectory)
        a1 = self.linklist[0].armLength 
        # 'inverse kinematics' of a 1-link planar robot:
        angle1 = np.arctan2(self.trajectory[self.i,1],self.trajectory[self.i,0])
        self.linklist[0].frame.setTransform(np.eye(4))
        self.linklist[0].frame.rotate(np.degrees(angle1),0,0,1)
        self.linklist[0].frame.translate(a1*np.cos(angle1),a1*np.sin(angle1),0)

        # ASSIGNMENT: replace the above angle calculation with that of a 2 link robot
        #               use kin_planar_inverse in rm_utils to calculate the angles           

        # now update the graphics in window w.
        self.w.update()

        # increase the counter, such that next time we get the next point
        self.i += 1
        # if the counter arives at N, reset it to 0, to repeat the trajectory
        if self.i==N: self.i=0

        # next line not really necessary, but its neat to return something
        return 


#start up the main
Main() 