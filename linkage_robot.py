"""
Simulation of a 2-link nonplanar robot.

The current version of this file only simulates a 1-link nonplanar robot.
Change it to simulate a 2-link nonplanar robot.

We have prepared already some functions and code-examples, but you have to put
every-thing together. Carefully read the code and the comments. The comments
are also written as an example of how to write code comments, that you should
always write in your own code as well. So, that your code is not just readable
to you right now, but also by your colleagues.
"""

# load the modules as discussed last week
from pyqtgraph.Qt import QtCore, QtWidgets
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import numpy as np

from rm_utils import *



class ArmLink:
    '''This class generates a link for a robot, complete with cillinders, boxen and 
    frames(frame_begin & frame). You don't have to change anything in here
    '''
    def __init__(self, id, DH, frame_attach) -> None:
        self.id = id
        self.armLength = DH["a"]
        # colors
        joint_a_color = (1,1,.3,1)
        joint_b_color = (1,.5,.2,1)
        link_color = (0.3,0.3,1,1)
        


        #creating parameters and the transformation matrix for link 1 using link function
        vertices_link,faces_link,vertices_joint_a,faces_joint_a,vertices_joint_b,faces_joint_b,self.H = link(DH["alpha"],DH["a"],DH["theta"],DH["d"],joint_a_radius=.7,joint_a_height=.5,joint_b_radius=.7,joint_b_height=.5,link_width=.5,link_height=0.1,Na=40,Nb=40,Nlink=30,Nround=16,calc_faces=True)

        # the frame of the link attaches to the previous frame (frame_attach), which becomes its parent frame.
        # it will move relative to that frame
        self.frame = gl.GLAxisItem(antialias=True,glOptions='opaque')
        self.frame.setTransform(self.H)
        self.frame.setGLOptions({'glLineWidth':(2,)})
        self.frame.setParentItem(frame_attach) # frame_attach instead of frame as parent!
        
        H_inv = np.linalg.inv(self.H)
        
        # frame_begin is a visual aid to show the relative motion compared to its parent.
        # you can outcomment it if you don't like it
        self.frame_begin = gl.GLAxisItem(antialias=True,glOptions='opaque')
        self.frame_begin.setParentItem(self.frame)
        self.frame_begin.setGLOptions({'glLineWidth':(2,)})
        self.frame_begin.setTransform(H_inv)

        self.joint_a = gl.GLMeshItem(vertexes=vertices_joint_a,faces=faces_joint_a,
            smooth=False,computeNormals=True,drawFaces=True,color=joint_a_color,shader='shaded',glOptions='opaque')
        self.link = gl.GLMeshItem(vertexes=vertices_link,faces=faces_link,
            smooth=False,computeNormals=True,drawFaces=True,color=link_color,shader='shaded',glOptions='opaque')

        self.joint_b = gl.GLMeshItem(vertexes=vertices_joint_b,faces=faces_joint_b,
            smooth=False,computeNormals=True,drawFaces=True,color=joint_b_color,shader='shaded',glOptions='opaque')

        # we specify the joint and arm relative to relative to frame, so that it will move with frame
        self.joint_a.setParentItem(self.frame)
        self.link.setParentItem(self.frame)
        self.joint_b.setParentItem(self.frame)
        self.joint_a.setTransform(H_inv)
        self.link.setTransform(H_inv)
        self.joint_b.setTransform(H_inv)

        return
    
class Robot:
    ''' 
    This is your Robot class. In it you will define your space and robot.
    '''   
    def __init__(self):
        self.w = gl.GLViewWidget()
        self.w.show()
        self.w.setWindowTitle('Non-planar 2-link robot simulation')
        self.w.opts['azimuth'] = -45
        self.w.setCameraPosition(distance=20)

        # create a grid-plane:
        g = gl.GLGridItem()
        # move the grid slightly to the back so the gridlines will not distort 
        # the axes of the coordinate frames
        g.translate(0,0,-0.1)
        self.w.addItem(g)

        # create frame0 (the fixed world-frame)
        frame0 = gl.GLAxisItem(antialias=True,glOptions='opaque')
        # we make the lines a bit thicker to stress this is the world-frame
        frame0.setGLOptions({'glLineWidth':(2,)})
        self.w.addItem(frame0)

        # link 1
        a_1     = 4
        alpha_1 = 45*(np.pi/180)
        d_1     = 1.5
        theta_1 = 30*(np.pi/180)
        DH_1 = dict(a = a_1, alpha = alpha_1, d = d_1, theta = theta_1)

        self.linklist = self.createRobot(frame0, DH_1, self.w)
        #ASSIGMENT: change the above two functions so they can receive both DH dictionaries
    
        return
    
    def createRobot(self,frame0, DH_1, w):
        base = self.createBase(frame0)
        w.addItem(base)

        link1 = ArmLink(1,DH_1, frame0)
        linkList =[link1]

        for L in linkList:
            w.addItem(L.joint_a)
            w.addItem(L.link)
            w.addItem(L.joint_b)
            w.addItem(L.frame_begin)
            w.addItem(L.frame)

        return linkList
    
    def createBase(self,frame):
        base_color = (0.3,0.3,0.3,1)
        base_width = 2
        base_length = 2
        base_height = 1
        vertices_base,faces_base = box((base_width,base_length,base_height))
        base = gl.GLMeshItem(vertexes=vertices_base,faces=faces_base,
            smooth=False,computeNormals=True, drawFaces=True,drawEdges=False, 
            shader='shaded',glOptions='opaque')
        base.opts['color'] = base_color 
        base.translate(-.5*base_width,-.5*base_length,-base_height)
        base.setParentItem(frame)
        return base
    
class Main:
    '''
    This is the main of you script. This will initialise the widget and start the timer. it also contains the update_window
    '''   
    def __init__(self):
        self.app = QtWidgets.QApplication([])
        self.Kenny = Robot() #Kenny is the name of this robot :), you can give it you own name if you like
        self.linkList = self.Kenny.linklist
      
        H_0_1 = self.linkList[0].H
        print("\nMatrix H_0_1 is: \n",H_0_1,"\n")
        #ASSIGMENT: print out the H of the other linkage
        #ASSIGMENT: compute and print H_0_2 

        print("De coordinates of frame 1 from frame 0 perpective is: ")
        print("x: ",round(H_0_1[0][3],2))
        print("y: ",round(H_0_1[1][3],2))
        print("z: ",round(H_0_1[2][3],2),"\n\n")
        #ASSIGMENT: print the end-effector coordinates of the 2-link robot 
        self.app.exec_()
        return
    

#starts up the main
Main()







